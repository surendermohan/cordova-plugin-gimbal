Cordova Beacon Gimbal plugin
=======================

Add to plist :
```
<key>UIBackgroundModes</key>
<array>
  <string>bluetooth-central</string>
  <string>bluetooth-peripheral</string>
</array>
```

# API

## Init plugin

```
var gimbal = cordova.require('cordova/plugin/gimbal');

gimbal.startService('GIMBAL_APP_ID', 'GIMBAL_APP_SECRET', 'GIMBAL_CALLBACK_URL');
```

## Configuration

```
// Enable local notification on Gimbal beacon detection
gimbal.configure.enableLocalNotification(true);
```

```
gimbal.configure.setLocalNotificationMessage('I found this beacon : %@');
```

```
gimbal.configure.setLocalNotificationRange(-50);
```

```
// One notification every 10 sec
gimbal.configure.setLocalNotificationMaxSpam(10);
```

```
gimbal.configure.setLocalNotificationCallback(function(data) {
    console.log('I\'m notified', data);
    // Even in background...
});
```

## Callbacks


```
gimbal.didArriveMonitoring(function() {
    console.log('Arrive');
});
gimbal.didDepartMonitoring(function() {
    console.log('Depart');
});
gimbal.didReceivedMonitoring(function(data) {
    console.log(data);
});
```