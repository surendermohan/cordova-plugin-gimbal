//
//  Gimbal.m
//  Beacon
//
//  Created by Lionel Penaud on 02/10/2014.
//
//

#import "Gimbal.h"
#import <Cordova/CDV.h>
#import <FYX/FYX.h>
#import <FYX/FYXVisitManager.h>
#import <FYX/FYXTransmitter.h>

@interface Gimbal () <FYXServiceDelegate, FYXVisitDelegate>

@property (nonatomic) FYXVisitManager *visitManager;

@property (nonatomic) BOOL configLocalNotificationEnable;
@property (nonatomic) NSString * configLocalNotificationMessage;
@property (nonatomic) int configLocalNotificationRange;
@property (nonatomic) int configLocalNotificationMaxSpam;
@property (nonatomic) NSString * configLocalNotificationCallbackId;

@property (nonatomic) NSString * _startCallbackId;
@property (nonatomic) NSString * _didArriveCallbackId;
@property (nonatomic) NSString * _didDepartCallbackId;
@property (nonatomic) NSString * _didReceivedSightingCallbackId;
@property (nonatomic) NSDate * lastAlertSent;

@end

@implementation Gimbal

-(id)init {
    if (self = [super init])  {
        // Defaults values
        self.configLocalNotificationEnable = YES;
        self.configLocalNotificationMessage = @"I'm close to %@";
        self.configLocalNotificationRange = -40;
        self.configLocalNotificationCallbackId = nil;
        self.configLocalNotificationMaxSpam = 5;
    }
    return self;
}

// Configuration
- (void)enableLocalNotification:(CDVInvokedUrlCommand*)command
{
    NSString* enabled = [NSString stringWithFormat:@"%@",[[command arguments] objectAtIndex:0]];
    self.configLocalNotificationEnable = [enabled isEqualToString:@"1"];
}

- (void)setLocalNotificationMessage:(CDVInvokedUrlCommand*)command
{
    self.configLocalNotificationMessage = [[command arguments] objectAtIndex:0];
}

- (void)setLocalNotificationRange:(CDVInvokedUrlCommand*)command
{
    NSString *range = [[command arguments] objectAtIndex:0];
    self.configLocalNotificationRange = [range integerValue];
}

- (void)setLocalNotificationMaxSpam:(CDVInvokedUrlCommand*)command
{
    NSString *range = [[command arguments] objectAtIndex:0];
    self.configLocalNotificationMaxSpam = [range integerValue];
}

- (void)setLocalNotificationCallback:(CDVInvokedUrlCommand*)command
{
    self.configLocalNotificationCallbackId = command.callbackId;
}


- (void)startService:(CDVInvokedUrlCommand*)command
{
    NSString *appId = [[command arguments] objectAtIndex:0];
    NSString *appSecret = [[command arguments] objectAtIndex:1];
    NSString *callbackUrl = [[command arguments] objectAtIndex:2];

    [FYX setAppId:appId
        appSecret:appSecret
      callbackUrl:callbackUrl];

    [FYX startService:self];

    self.visitManager = [FYXVisitManager new];
    self.visitManager.delegate = self;
    [self.visitManager start];

    [FYX startService:self];

    //NSLog(@"CallbackId : %@", command.callbackId);
    self._startCallbackId = command.callbackId;

    self.lastAlertSent = nil;
}

- (void)didArriveMonitoring:(CDVInvokedUrlCommand*)command
{
    self._didArriveCallbackId = command.callbackId;
}

- (void)didDepartMonitoring:(CDVInvokedUrlCommand*)command
{
    self._didDepartCallbackId = command.callbackId;
}

- (void)didReceivedMonitoring:(CDVInvokedUrlCommand*)command
{
    self._didReceivedSightingCallbackId = command.callbackId;
}

- (void)serviceStarted
{
    self.visitManager = [FYXVisitManager new];
    self.visitManager.delegate = self;
    [self.visitManager start];

    CDVPluginResult* pluginResult = nil;
    pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:@"ServiceSuccessfullyStarted"];
    //NSLog(@"FYX Service Successfully Started");
    [self.commandDelegate sendPluginResult:pluginResult callbackId:self._startCallbackId];



    if ([UIApplication instancesRespondToSelector:@selector(registerUserNotificationSettings:)]){
        [[UIApplication sharedApplication] registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:UIUserNotificationTypeAlert|UIUserNotificationTypeBadge|UIUserNotificationTypeSound categories:nil]];
    }

    //    UIUserNotificationSettings* notificationSettings = [UIUserNotificationSettings settingsForTypes:UIUserNotificationTypeAlert categories:nil];
    //    [[UIApplication sharedApplication] registerUserNotificationSettings:notificationSettings];

    UIUserNotificationSettings* badgeSettings = [UIUserNotificationSettings settingsForTypes:UIUserNotificationTypeBadge categories:nil];
    [[UIApplication sharedApplication] registerUserNotificationSettings:badgeSettings];

    [[UIApplication sharedApplication] setApplicationIconBadgeNumber:0];

}

- (void)startServiceFailed:(NSError *)error
{
    // this will be called if the service has failed to start
    CDVPluginResult* pluginResult = nil;
    pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsString:@"ServiceStartFailed"];
    NSLog(@"FYX Service failed to Start");
    NSLog(@"%@", error);
    [self.commandDelegate sendPluginResult:pluginResult callbackId:self._startCallbackId];
}

- (void)didArrive:(FYXVisit *)visit;
{
    // this will be invoked when an authorized transmitter is sighted for the first time
    //NSLog(@"I arrived at a Gimbal Beacon!!! %@", visit.transmitter.name);

    CDVPluginResult* pluginResult = nil;
    pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:visit.transmitter.name];

    [pluginResult setKeepCallback:[NSNumber numberWithBool:YES]];
    [self.commandDelegate sendPluginResult:pluginResult callbackId:self._didArriveCallbackId];
}

- (void)receivedSighting:(FYXVisit *)visit updateTime:(NSDate *)updateTime RSSI:(NSNumber *)RSSI;
{
    // this will be invoked when an authorized transmitter is sighted during an on-going visit
    NSDictionary *dicResult = @{
                                @"name": visit.transmitter.name,
                                @"rssi": RSSI,
                                @"identifier": visit.transmitter.identifier,
                                @"temperature": visit.transmitter.temperature,
                                @"battery": visit.transmitter.battery
                                };
    CDVPluginResult* pluginResult = nil;
    pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsDictionary:dicResult];

    [pluginResult setKeepCallback:[NSNumber numberWithBool:YES]];
    [self.commandDelegate sendPluginResult:pluginResult callbackId:self._didReceivedSightingCallbackId];

    NSTimeInterval diff = self.configLocalNotificationMaxSpam+1;

    if (self.lastAlertSent != nil) {
        diff = [[NSDate date] timeIntervalSinceDate:self.lastAlertSent];
    }
    if (self.configLocalNotificationEnable && [RSSI intValue] > self.configLocalNotificationRange && diff > self.configLocalNotificationMaxSpam) {

        [self.commandDelegate sendPluginResult:pluginResult callbackId:self.configLocalNotificationCallbackId];

        UILocalNotification* localNotification = [[UILocalNotification alloc] init];
        localNotification.fireDate = [NSDate date];
        localNotification.alertBody = [NSString stringWithFormat:self.configLocalNotificationMessage, visit.transmitter.name];
        localNotification.timeZone = [NSTimeZone defaultTimeZone];

        [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
        [[UIApplication sharedApplication] setApplicationIconBadgeNumber:1];

        self.lastAlertSent = localNotification.fireDate;

    }
}
- (void)didDepart:(FYXVisit *)visit;
{
    // this will be invoked when an authorized transmitter has not been sighted for some time
    //NSLog(@"I left the proximity of a Gimbal Beacon!!!! %@", visit.transmitter.name);
    //NSLog(@"I was around the beacon for %f seconds", visit.dwellTime);

    CDVPluginResult* pluginResult = nil;
    pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:visit.transmitter.name];

    [pluginResult setKeepCallback:[NSNumber numberWithBool:YES]];
    [self.commandDelegate sendPluginResult:pluginResult callbackId:self._didDepartCallbackId];
}

@end
